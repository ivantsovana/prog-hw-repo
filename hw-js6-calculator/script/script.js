var numberBtns = document.querySelectorAll(".number");
var signBtns = document.querySelectorAll(".sign");
var clearBtn = document.querySelector(".clear");
var toggleSignBtn = document.querySelector(".toggle-sign");
var resultBtn = document.querySelector(".result");
var percentBtn = document.querySelector(".percent");
var inp = document.querySelector(".calculator__input");
var body = document.querySelector("body");

for (i = 0; i < numberBtns.length; i++){
  numberBtns[i].addEventListener("click", function () {
     inp.value += this.innerText;
  });
}

for (i = 0; i < signBtns.length; i++){
  signBtns[i].addEventListener("click", checkSign);
}

function checkSign() {
    if (!isNaN(inp.value[inp.value.length-1])) {
      inp.value += this.innerText;
  }
}

resultBtn.addEventListener("click", getResult);

function getResult() {
  inp.value = eval(inp.value);
}

clearBtn.addEventListener("click", function () {
  inp.value = "";
})

toggleSignBtn.addEventListener("click", function () {
 inp.value = (-1)*eval(inp.value);
})

percentBtn.addEventListener("click", function () {
  inp.value = eval(inp.value) * 0.01;
})

body.addEventListener("keydown", checkKey);

function checkKey(e) {
  if (e.key == "Enter") {
    getResult();
  };
}