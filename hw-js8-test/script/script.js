let minutes = 40;
let seconds = 0;
const minutesSpan = document.querySelector("#minutes");
const secondsSpan = document.querySelector("#seconds");

function timer() {

  if (seconds === 0) {
    if (minutes === 0) {
      clearInterval(timerid);
      alert("Time is out");
      return minutes = 0;

    } else {
      seconds = 59;
      minutes -= 1;
    }
  } else {
    seconds -= 1;
  }

  function formatNum(num) {
    if (num < 10) {
      return (`0${num}`);
    } else {
      return num;
    }
  }

  minutesSpan.innerText = `${formatNum(minutes)}`;
  secondsSpan.innerText = `${formatNum(seconds)}`;
}

let timerid;

const timerBtn = document.querySelector(".timer__btn")

timerBtn.addEventListener("click", startCountdown);
function startCountdown() {
  timerid = setInterval(timer, 1000);
  timerBtn.classList.add("hidden");
  document.querySelector(".form-container").classList.remove("hidden");
  document.querySelector(".timer__block").classList.remove("hidden");

}

// рекурсія

//  Создать функцию, возводящую число в степень, число и сама степень вводится пользователем

function mathPow() {
  const x = +prompt("Яке число ви хочете піднести в степінь?");
  const a = +prompt("Введіть степінь");


  function powX(x, a) {
    if (a == 1) {
      return x;
    } else {
      return x * powX(x, a - 1);
    }
  }
  let result = powX(x, a);
  console.log(result);
}

mathPow();

