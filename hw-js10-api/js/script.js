const dateDiv = document.querySelector(".calendar__date");
const holidaysDiv = document.querySelector(".calendar__holidays");
const apiKey = '983cbe9cb1d14d1fc266b021c6351fee0284f706';
// api documentation https://calendarific.com/api-documentation
let now = new Date;
const apiUrl = `https://calendarific.com/api/v2/holidays?&api_key=${apiKey}&&year=${now.getFullYear()}&country=UA&month=${now.getMonth()}`
const months = [
  "jan",
  "feb",
  "mar",
  "apr",
  "may",
  "jun",
  "jul",
  "aug",
  "sep",
  "oct",
  "nov",
  "dec",
];

const todayDate = `Today is ${now.getDate()} ${months[now.getMonth()]} ${now.getFullYear()} `;

dateDiv.innerHTML = (`<h1 > ${todayDate}</h1> `);

let data;

fetch(apiUrl)
  .then((response) => response.json())
  .then(json => data = json);

let dataArr;
setTimeout(function () {
  dataArr = data.response.holidays;
  dataArr.forEach(function (el) {
    let item = `<div class="calendar__item">
        <div class="calendar__date">${el.date.datetime.day} ${months[el.date.datetime.month]} ${el.date.datetime.year}</div>
        <div class="calendar__name">${el.name}</div>
        <div class="calendar__type">${el.primary_type}</div>
        <div class="calendar__description">${el.description}</div>
      </div>
    `
    holidaysDiv.innerHTML += item;
  })

  const type = document.querySelectorAll(".calendar__type");
  type.forEach(function (el) {
    switch (el.innerText) {
      case "Suspended National Holiday":
        el.parentElement.classList.add("blue");
        break;
      case "Orthodox":
        el.parentElement.classList.add("red");
        break;
      case "Observance":
        el.parentElement.classList.add("yellow");
        break;
    }
    console.log(el);
  })
}, 1000);


