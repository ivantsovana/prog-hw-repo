var tabNavigationLinks = document.querySelectorAll(".tab__title");
var tabContentDivs = document.querySelectorAll(".tab__item");
var htmlLink = document.querySelector("#tab__html-link");
var cssLink = document.querySelector("#tab__css-link");
var jsLink = document.querySelector("#tab__js-link");
var htmlContent = document.querySelector("#tab__html-content");
var cssContent = document.querySelector("#tab__css-content");
var jsContent = document.querySelector("#tab__js-content");

for (var i = 0; i < tabNavigationLinks.length; i++){
  tabNavigationLinks[i].addEventListener("click", makeActiveButton);
}
 
function makeActiveButton() {
  for (var i = 0; i < tabNavigationLinks.length; i++){
    tabNavigationLinks[i].classList.remove("tab__title_active")
  }
  this.classList.add("tab__title_active");
}

function removeActiveClass() {
  for (let i = 0; i < tabContentDivs.length; i++){
    tabContentDivs[i].classList.remove("tab__item_active");
  }
}

htmlLink.addEventListener("click", displayHtmlTab);
cssLink.addEventListener("click", displayCssTab);
jsLink.addEventListener("click", displayJsTab);

function displayHtmlTab() {
  removeActiveClass();
  htmlContent.classList.add("tab__item_active");
}

function displayCssTab() {
  removeActiveClass();
  cssContent.classList.add("tab__item_active");
}

function displayJsTab() {
  removeActiveClass();
  jsContent.classList.add("tab__item_active");
}