/* Алгоритм взаємодії з клієнтом наступний: 
В налаштуваннях цієї програми необхідно вводити одну константу мінімальну заробітну плату,
 це десь на адміністративній частині сайту, що недоступна користувачу.
Ось дещо структурований алгоритм взаємодії
Оберіть характер стягнення:
варіанти: 
1. майнове                         2. немайнове
1.1        Якщо майнове то: введіть суму боргу, яку необхідно стягнути,
1.1.1     якщо 2% від суми боргу менше або дорівнює 10 мінімальним заробітним платам 
          то розмір авансового внеску дорівнює 2% від суми, яку необхідно стягнути
1.1.2     якщо 2% від суми боргу більше 10 мін. заробітних плат то сума 
          авансового внеску дорівнює 10 мінімальним заробітним платам.
 
2.1 Якщо немайнове, то питання: зазначте будь ласка стягувача із запропонованих варіантів 
2.1.1     стягувачем є фізична особа (громадянин)  наслідок розмір авансового внеску 
1 мінімальна  заробітна плата програма розраховує
2.1.2     Стягувачем є юридична особа (підприємство, установа, організація) 
 наслідок розмір авансового внеску 2 мінімальні заробітні плати  програма розраховує  -->
*/
const typeOfCreditorDiv = document.querySelector("#form__type-of-creditor");
const debtInp = document.querySelector("#form__inp-sum-of-debt");
const debtInpDiv = document.querySelector("#debt-inp");
const comissionOutput = document.querySelector("#form__comission-output");
const propertyDebt = document.querySelector("#property");
const nonPropertyDebt = document.querySelector("#non-property");
const personal = document.querySelector("#personal");
const corporate = document.querySelector("#corporate");
const buttons = document.querySelectorAll(".form__btn");
const fullName = document.querySelector("#form__full-name");
const adress = document.querySelector("#form__adress");
const minWaige = 6700;

propertyDebt.addEventListener("change", toggleFormStructure);
nonPropertyDebt.addEventListener("change", toggleFormStructure);

function toggleFormStructure() {
  typeOfCreditorDiv.classList.toggle("hidden");
  debtInpDiv.classList.toggle("hidden");
  comissionOutput.innerText = "";
}

debtInp.addEventListener("input", updateDebtSum);
personal.addEventListener("change", updateDebtSum);
corporate.addEventListener("change", updateDebtSum);
fullName.addEventListener("input", updateFullName);
adress.addEventListener("input", updateAdress);

function updateFullName() {
  localStorage.setItem("fullName", fullName.value);
}

function updateAdress() {
  localStorage.setItem("adress", adress.value);
}


function updateDebtSum() {
  // debtSum = debtInp.value;
  if (propertyDebt.checked) {
    if (!isNaN(debtInp.value)) {
      comissionOutput.classList.remove("red-font");
      if (debtInp.value * 0.002 <= minWaige) {
        comissionOutput.innerText = (debtInp.value * 0.02).toFixed(2);
        localStorage.setItem("cost", (debtInp.value * 0.02).toFixed(2))
      } else {
        comissionOutput.innerText = (10 * minWaige).toFixed(2);
        localStorage.setItem("cost", (10 * minWaige).toFixed(2));
      }
    } else {
      comissionOutput.innerText = 'Сума боргу має бути числом!';
      comissionOutput.classList.add("red-font");
    }
  } else {
    if (personal.checked) {
      comissionOutput.innerText = minWaige;
      localStorage.setItem("cost", minWaige)
    } else {
      comissionOutput.innerText = minWaige * 2;
      localStorage.setItem("cost", minWaige * 2);
    }
  }
}
