const listItem = document.querySelectorAll(".app__tasklist li");
const list = document.querySelector(".app__tasklist");
const removeBtn = document.querySelectorAll(".app__remove-btn");
const formInp = document.querySelector("#app__input");
const inputBtn = document.querySelector(".app__inp-btn");
let doneTask = [];
const noTask = document.querySelector(".app_noTask");

inputBtn.addEventListener("click", function () {
  if (formInp.value) {
    const li = document.createElement("li");
    li.innerText = formInp.value;
    li.classList.add("app__list-item");
    formInp.value = "";
    submitTask(li);
    list.appendChild(li);
  }
})

for (i = 0; i < listItem.length; i++) {
  submitTask(listItem[i]);
}

if (listItem.length < 1) {
  app_noTask.innerText = "No any task "
}

function submitTask(task) {
  task.addEventListener("click", function () {
    task.classList.toggle("done");
  });

  let span = document.createElement("span");
  span.innerText = "x";
  span.classList.add("app__remove-btn");
  span.addEventListener("click", function () {
    this.parentElement.remove();
  })
  task.appendChild(span);
}
