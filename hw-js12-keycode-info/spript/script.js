let loaded = false;
let keyData = {};
let dataStorage = [];
const keyInfoDiv = document.querySelector(".app__keyInfo"),
  callToActionDiv = document.querySelector(".app__callToAction"),
  appCardsDiv = document.querySelector(".app__cards"),
  cardsData = {
    headerTitle: document.querySelector(".app__headerTitle"),
    keyCodeTitle: document.querySelector(".app__keyCode"),
    key: document.querySelector("#app__cardKey"),
    code: document.querySelector("#app__cardCode"),
    which: document.querySelector("#app__cardWhich")
  },
  appHistory = document.querySelector(".app__history");
let repeatSymbol = [false];

document.body.addEventListener("keydown", getEventData);

function getEventData(e) {
  if (!loaded) {
    loaded = true;
    toggleVisibility();
  }


  let keyCaps = e.key;
  // перетворюємо у заголовні лише ті значення ключів, які літери
  if (typeof keyCaps == "string") { keyCaps = keyCaps.toUpperCase() };
  keyData = {
    keyCode: e.keyCode,
    key: e.key,
    keyCaps: keyCaps,
    code: e.code,
    which: e.which
  };

  //якщо поточний символ збігається з якимось, який вже є у архіві(один із 4 останніх), то видаляємо збіг і добавляємо поточний елемент на початок

  if (dataStorage.length > 0) {
    for (let i = 0; i < dataStorage.length; i++) {
      if (dataStorage[i].which == keyData.which) {
        dataStorage.splice(i, 1);
      }
    }
  }

  // добавляємо інформацію про поточну клавішу, видаляємо зайву
  dataStorage.unshift(keyData);
  if (dataStorage.length > 4) { dataStorage.pop() };
  showKeyInfo();
}


// функція для оновлення даних на сторінці
function showKeyInfo() {
  // console.log(keyData);
  cardsData.headerTitle.innerHTML = `JavaScript Key Code ${keyData.keyCode}`
  cardsData.keyCodeTitle.innerHTML = keyData.keyCode;
  cardsData.key.innerHTML = keyData.key;
  cardsData.code.innerHTML = keyData.code;
  cardsData.which.innerHTML = keyData.which;
  appHistory.innerHTML = "";
  for (let i = 0; i < dataStorage.length; i++) {
    let div = document.createElement("div");
    div.innerText = `${dataStorage[i].keyCaps}`;
    div.classList.add("app__historyEl");
    div.addEventListener("click", updateData);
    appHistory.insertAdjacentElement("beforeend", div);
  }
}

// якщо натискаємо кнопку, то змінюємо інформацію на сторінці, порівнюючи текст "кнопки" і значення keyCaps в масиві
function updateData() {
  // console.log(this.innerText);
  dataStorage.forEach(el => {
    // console.log(el);
    let text = this.innerText;
    if (el.keyCaps == this.innerText || (el.keyCaps == " " && text == "")) {
      keyData = {
        keyCode: el.keyCode,
        key: el.key,
        keyCaps: this.innerText,
        code: el.code,
        which: el.which
      };
      // console.log(keyData);
      showKeyInfo();
    }
  }
  )
}

function toggleVisibility() {
  keyInfoDiv.classList.toggle("hidden");
  callToActionDiv.classList.toggle("hidden");
  appCardsDiv.classList.toggle("hidden");
}