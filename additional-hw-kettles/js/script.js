var bgColorsArr = [
  "#95CFD5",
  "#C42129",
  "#E0ADC5",
  "#F5C7B3",
];

var sliderList = document.querySelector(".main__preview-slider-list").children;
var main = document.querySelector("#main");
var productImage = document.querySelector("#productImage");

function changeDesign(index, color) {
   for (var i = 0; i < sliderList.length;i++){
     sliderList[i].classList.remove("main__preview-item_selected");
  }
  document.querySelector(`#main__${color}-radio`).checked = true;
  document.querySelector(`.main__item-holder-${color}`).classList.add("main__preview-item_selected");
  document.querySelector("#main").style.backgroundColor= `${bgColorsArr[index]}`;
  productImage.src = `./img/${color}-kettle.png`
}

function changeDesignBlue() {
  changeDesign(0, "blue");
}

function changeDesignRed() {
  changeDesign(1, "red");
}

function changeDesignPink() {
  changeDesign(2, "pink");
}

function changeDesignBeige() {
  changeDesign(3, "beige");
}

document.querySelector("#main__blue-kettle-preview").addEventListener("click", changeDesignBlue);
document.querySelector("#main__red-kettle-preview").addEventListener("click", changeDesignRed);
document.querySelector("#main__pink-kettle-preview").addEventListener("click", changeDesignPink);
document.querySelector("#main__beige-kettle-preview").addEventListener("click", changeDesignBeige);


document.querySelector("#main__blue-radio").addEventListener("click", changeDesignBlue);
document.querySelector("#main__red-radio").addEventListener("click", changeDesignRed);
document.querySelector("#main__pink-radio").addEventListener("click", changeDesignPink);
document.querySelector("#main__beige-radio").addEventListener("click", changeDesignBeige);
