import React from "react"

function Task ({task}) {
    const {id, title, completed} = task;
    const data = `${new Date().getFullYear()}-${new Date().getMonth() + 1 > 10 ? new Date().getMonth()+1 : "0"+(new Date().getMonth()+1)}-${new Date().getDate()}  ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getMilliseconds()}`
   
    return(
        <tr>
            <td>{id}</td>
            <td>{title}</td>
            <td>{completed? "Успішно виконано ✅" : <button type="button">позначити виконаним  ✍️</button>}</td>
            <td>{data}</td>
        </tr>
    )
}

export default Task;