import React from "react";
import { Thead } from "./thead/thead";
import Task from "./task/task";
import "./todo-list.css"


const TodoList = ({data}) => {
    const headers = ["№", "Назва задачі", "Статус", "Дата створення"];
    return (
        <table lang='uk'>
            <Thead headers={headers}></Thead>
            <tbody> 
                {
                   Array.isArray(data)? data.map((e,i)=>{
                       return  <Task task={e}></Task>
                   })  : null
                }
            </tbody>
        </table>
    )
}

export default TodoList;