import React from "react";

export const Thead = ({headers}) => {

    return(
        <thead>
            <tr>
                {Array.isArray(headers)? 
                headers.map((td, ind)=>{
                    return <th key={Math.random()*100+ind+"thead"}>{td}</th>
                })
                : <div className="error">Помилка</div>}
            </tr>
        </thead>
    )
}