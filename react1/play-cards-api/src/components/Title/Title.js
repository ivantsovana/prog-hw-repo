import React from "react";
import "./Title.css";
// import img from "..."

export default function Title() {
  return <div className="Title">
    <img className="Title__logoImg" src="../../img/Logo.png" alt="Star Wars Logo" />
  </div>
}