import './App.css';
import Cards from "../Cards/Cards";
import Title from "../Title/Title";


export default function App() {


  return (
    <div className="App">
      <Title/>
      <Cards/>
    </div>
  );

}

