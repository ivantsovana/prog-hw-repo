import React from "react";
import "./Card.css"

export default function Card({ data }) {
  const { name, birth_year, height, mass, gender, hair_color, url } = data;

  return (<div className="Card" >
    <h2 className="Card__title">{name}</h2>  
    <div className="Card__imgWrapper">
      <img className="Card__img" src="../../../img/Logo.png" alt="" />
    </div>
    <ul className="Card__list">
      <li className="Card__listItem">
        <span>Birth year:</span>
        <span> {birth_year}</span>
        </li>
      <li className="Card__listItem">
        <span>Height:</span>
        <span>{height}</span>
        </li>
      <li className="Card__listItem">
        <span>Mass:</span>
        <span>{mass}</span>
        </li>
      <li className="Card__listItem">
        <span>Gender:</span>
        <span>{gender}</span>
        </li>
      <li className="Card__listItem">
        <span>Hair color:</span>
        <span>{hair_color}</span>
        </li>
      <li className="Card__listItem Card__linkWrapper"><a className="Card__link" target={"_blank"} rel="noreferrer" href={url}>More info</a> </li>
    </ul>
  </div>)
      
}