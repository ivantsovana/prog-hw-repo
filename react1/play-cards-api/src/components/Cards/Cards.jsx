import React from "react";
import { list } from "../../data/list";
import "./Cards.css";
import Card from "./Card/Card";

export default function Cards() {
  console.log(list);
  // {name,birth_year, height,mass, gender,hair_color,url}

  return<div className="Cards">
          
      {list.map((el, index) => {
        return <Card data={el} key={index}/>
      })}
  
  </div>
  }
