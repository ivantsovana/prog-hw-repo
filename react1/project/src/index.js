import React from 'react';
import ReactDom from "react-dom";
import data from "./data";

function App() {
  return (
    <div>
      <h1>Курс валют</h1>
      <table border={1}>
        <thead>
          <tr>
            <th>Назва валюти</th>
            <th>Обмінний курс</th>

          </tr>
        </thead>
        <tbody>
          {data.map(element => {
            return (
              <tr key={element.r030}>
                <td>
                  {element.txt}
                </td>
                <td>
                  {element.rate}
                </td>

              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

ReactDom.render(<App />, document.getElementById("wrapper"));
console.dir(data);