let data;
const app = document.querySelector(".data"),
  title = document.querySelector(".header__title"),
  appCurrency = document.querySelectorAll(".app__select"),
  appCurrency1 = document.querySelector("#app__currency1"),
  appCurrency2 = document.querySelector("#app__currency2"),
  form = document.querySelector("#form"),
  inp = document.querySelector(".app__inp"),
  result = document.querySelector(".app__result"),
  toggleButton = document.querySelector("#app__toggleButton"),
  autoFillDiv = document.querySelectorAll(".app__autoFill");
let chosenCurrency1 = [];
let chosenCurrency2 = [];
let index;
let temporaryNamePlaceholder;



// ------------- асинхронна функція ---------------

async function getData() {
  const response = await fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
  data = await response.json();
  // console.log(data);
  for (let i = 0; i < data.length; i++) {
    data[i].fullName = data[i].cc + " " + data[i].txt;
  }
  displayRate();
}

getData();

// показуємо курс валют у таблиці
function displayRate() {
  // console.log(data);
  title.innerHTML = `Курс валют станом на <i>${data[0].exchangedate}</i >`;
  data.forEach(element => {

    // console.log(element.cc, element.rate, element.txt);

    app.insertAdjacentHTML("beforeend",
      `
  <div class= "data__item" >
          <div class="data__cc">${element.cc}</div>
          <div class="data__txt">${element.txt}</div>
          <div class="data__rate">${element.rate}</div>
        </div>
  `);
  });
}

appCurrency.forEach(el => {
  el.addEventListener("input", showSuggestions);
})


// ф-я для показу suggested назв валют
function showSuggestions() {
  // autoFill.innerHTML = "";
  // console.log("-------------");
  inp.value = "";
  index = 0;
  temporaryNamePlaceholder = "";
  this.nextElementSibling.innerHTML = "";
  if (this.value.length > 0) {
    data.forEach(el => {

      if (el.fullName.toLowerCase().includes(this.value.toLowerCase())) {
        // console.log(el.fullName);
        let div = document.createElement("div");
        div.classList.add("app__suggestedEl");
        div.innerText = el.fullName;
        div.addEventListener("click", submitValue);
        this.nextElementSibling.appendChild(div);
      }
    })
    this.nextElementSibling.firstChild.classList.add("app--hoverEl");
    temporaryNamePlaceholder = this.nextElementSibling.firstChild.innerText;
  }
}

// функція для присвоєння полю інпуту значення натиснутого suggested div

function submitValue() {
  this.parentElement.previousElementSibling.value = this.innerText;
  this.parentElement.innerHTML = "";
}

// при натисканні кнопки тогл, змінюю значення полів назви валют

toggleButton.addEventListener("click", toggleCurrency);
function toggleCurrency() {
  [appCurrency1.value, appCurrency2.value] = [appCurrency2.value, appCurrency1.value];
  getResult();
}

inp.addEventListener("input", getResult);

// form.addEventListener("change", getResult);


// при натисканні enter сторінка не має перезавантажуватись

form.addEventListener("submit", (e) => {
  e.preventDefault();
});

//функція для розрахунку курсу

function getResult() {
  result.innerText = "";
  chosenCurrency1 = [];
  chosenCurrency2 = [];
  data.forEach(el => {
    if (appCurrency1.value == el.fullName) {
      chosenCurrency1 = [el.rate, el.cc];
    }
    if (appCurrency2.value == el.fullName) {
      chosenCurrency2 = [el.rate, el.cc];
    }
    if (chosenCurrency1.length > 0 && chosenCurrency2.length > 0 && inp.value.length > 0) {
      result.innerText = (chosenCurrency1[0] / chosenCurrency2[0] * inp.value).toFixed(2) + " " + chosenCurrency2[1];
    }
  })
}

//Видаляю div з suggested elements при натисканні вільного місця

document.body.addEventListener("click", () => {
  autoFillDiv.forEach(el => {
    el.innerHTML = "";
  })
})

document.body.addEventListener("keydown", (e) => {
  // console.log(e);
  if (e.key == "ArrowDown" || e.key == "ArrowUp") {
    autoFillDiv.forEach(element => {
      if (element.children.length > 0) {
        let suggestedElements = element.children;
        // console.log(suggestedElements);

        for (let i = 0; i < suggestedElements.length; i++) {
          suggestedElements[i].classList.remove("app--hoverEl");
        }


        if (e.key == "ArrowDown") {
          index++;
          if (index > suggestedElements.length - 1) {
            index = 0;
          }
          // console.log(index);

        }

        if (e.key == "ArrowUp") {
          index--;
          if (index < 0) {
            index = suggestedElements.length - 1;
          }
          // console.log(index);

        }


        temporaryNamePlaceholder = suggestedElements[index].innerText;

        suggestedElements[index].classList.add("app--hoverEl");

        let length = suggestedElements.length;
        // console.log(suggestedElements);
      }
    })
  }
  if (e.key == "Enter") {
    autoFillDiv.forEach(element => {
      if (element.children.length > 0 && temporaryNamePlaceholder.length > 0) {
        element.previousElementSibling.value = temporaryNamePlaceholder;
        element.innerHTML = "";
        temporaryNamePlaceholder = "";
      }
    }
    )
  }

  if (e.key == "Escape") {
    autoFillDiv.forEach(element => {
      if (element.children.length > 0) {
        element.innerHTML = "";
        element.previousElementSibling.value = "";
      }
    })

  }
})