let data;
const app = document.querySelector(".data"),
  title = document.querySelector(".header__title"),
  appCurrency1 = document.querySelector("#app__currency1"),
  currency1Container = document.querySelector(".app__currency1Container"),
  сurrency2Container = document.querySelector(".app__currency2Container"),
  form = document.querySelector("#form"), inp = document.querySelector(".app__inp"),
  result = document.querySelector(".app__result"),
  toggleButton = document.querySelector("#app__toggleButton"),
  selectContainer = document.querySelector(".app__selectContainer");
let appCurrency2;
let chosenCurrency1;
let chosenCurrency2;
let chosenCurrencyCode;


// -------------------Перший варіант з fetch--------------------------

// fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
//   .then(response => response.json())
//   .then(json => data = json);

// setTimeout(function () {
//   // console.log(data);
//   title.innerHTML = `Курс валют станом на <i>${data[0].exchangedate}</i >`;
//   data.forEach(element => {

//     // console.log(element.cc, element.rate, element.txt);
//     app.insertAdjacentHTML("beforeend",
//       `
//   <div class= "data__item" >
//           <div class="data__cc">${element.cc}</div>
//           <div class="data__txt">${element.txt}</div>
//           <div class="data__rate">${element.rate}</div>
//         </div>
//   `);

//     let option = document.createElement("option");
//     option.innerHTML = element.cc + " " + element.txt;
//     appCurrency1.appendChild(option);
//   });


//   appCurrency2 = appCurrency1.cloneNode(true);
//   appCurrency2.id = "app__currency1";
//   appCurrency2.class = "app__select";
//   appCurrency2.addEventListener("change", () => {
//     result.innerText = "";
//     data.forEach(el => {
//       if (appCurrency2.options[appCurrency2.selectedIndex].innerHTML.includes(el.cc)) {
//         chosenCurrency2 = el.rate;
//         chosenCurrency2Code = el.cc;
//       }
//     })
//     if (appCurrency2.options[appCurrency2.selectedIndex].innerHTML.includes("UAH")) {
//       chosenCurrency2 = 1;
//       chosenCurrency2Code = "UAH";
//     }
//   })
//   сurrency2Container.insertAdjacentElement("beforeend", appCurrency2);

// }, 1000);


//-------------------Замінила fetch на async function---------------------------------------


// ------------- асинхронна функція ---------------

async function getData() {
  const response = await fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
  data = await response.json();
  console.log(data);
  displayRate()
}

getData();


//-----------------викликаємо функцію всередині асинхронної-----------------------

function displayRate() {
  // console.log(data);
  title.innerHTML = `Курс валют станом на <i>${data[0].exchangedate}</i >`;
  data.forEach(element => {

    // console.log(element.cc, element.rate, element.txt);

    app.insertAdjacentHTML("beforeend",
      `
  <div class= "data__item" >
          <div class="data__cc">${element.cc}</div>
          <div class="data__txt">${element.txt}</div>
          <div class="data__rate">${element.rate}</div>
        </div>
  `);

    let option = document.createElement("option");
    option.innerHTML = element.cc + " " + element.txt;
    appCurrency1.appendChild(option);
  });


  appCurrency2 = appCurrency1.cloneNode(true);
  appCurrency2.id = "app__currency1";
  appCurrency2.class = "app__select";
  appCurrency2.addEventListener("change", () => {
    result.innerText = "";
    data.forEach(el => {
      if (appCurrency2.options[appCurrency2.selectedIndex].innerHTML.includes(el.cc)) {
        chosenCurrency2 = el.rate;
        chosenCurrency2Code = el.cc;
      }
    })
    if (appCurrency2.options[appCurrency2.selectedIndex].innerHTML.includes("UAH")) {
      chosenCurrency2 = 1;
      chosenCurrency2Code = "UAH";
    }
  })
  сurrency2Container.insertAdjacentElement("beforeend", appCurrency2);

}

appCurrency1.addEventListener("change", () => {
  result.innerText = "";
  data.forEach(el => {
    if (appCurrency1.options[appCurrency1.selectedIndex].innerHTML.includes(el.cc)) {
      chosenCurrency1 = el.rate;
      chosenCurrency1Code = el.cc;
    };
  })
  if (appCurrency1.options[appCurrency1.selectedIndex].innerHTML.includes("UAH")) {
    chosenCurrency1 = 1;
    chosenCurrency1Code = "UAH";
  }
})


inp.addEventListener("input", getResult);

function getResult() {
  result.innerText = "";
  if (chosenCurrency1 && chosenCurrency2) {
    if (selectContainer.classList.contains("reverse")) {
      result.innerText = (chosenCurrency2 / chosenCurrency1 * inp.value).toFixed(2) + " " + chosenCurrency1Code;

    } else {
      result.innerText = (chosenCurrency1 / chosenCurrency2 * inp.value).toFixed(2) + " " + chosenCurrency2Code;
    }
  }
}

toggleButton.addEventListener("click", toggleCurrency);
function toggleCurrency() {

  selectContainer.classList.toggle("reverse");
  result.innerText = "";
  getResult();
}