/**
 * 2. Зробіть футбольне поле, можна використати власну верстку або взяти будь-яку картинку поля з Інтернету.
Додайте на поле зображення м'яча.
-Зробіть м'яч за допомогою події драг і дроп перетягуємим по всьому полю. 
-Зробіть таблицю рахунку(в залежності від дропу в якісь ворота - збільшується рахунок відповідної команди).
-Зробіть коригування дропу м'яча, на властивість offset(див. лекцію).
3.У файлі драг і дроп зробіть коригування дропу зображення, на властивість offset(див. лекцію).
 */

const leftField = document.querySelector(".football__fieldLeft");
const rightField = document.querySelector(".football__fieldRight");
const ball = document.querySelector(".ball");
const scoreLeft = document.querySelector(".football_scoreLeft");
const scoreRight = document.querySelector(".football_scoreRight");

const leftGoal = document.querySelector(".football__goalLeft");
const rightGoal = document.querySelector(".football__goalRight");

let score = [0, 0];


leftField.addEventListener("dragover", preventEvent);
rightField.addEventListener("dragover", preventEvent);

function preventEvent(e) {
  // console.log(e);
  e.preventDefault();
}

leftField.addEventListener("drop", dropElement);
rightField.addEventListener("drop", dropElement);

rightGoal.addEventListener("drop", addRightGoal);
leftGoal.addEventListener("drop", addLeftGoal);

function addLeftGoal() {
  score[0] += 1;
  scoreLeft.innerText = score[0];
}

function addRightGoal() {
  score[1] += 1;
  scoreRight.innerText = score[1];
}

function dropElement(e) {
  console.log(e);
  ball.style.left = e.layerX - ball.offsetWidth / 2 + "px";
  ball.style.top = e.layerY - ball.offsetHeight / 2 + "px";
  this.appendChild(ball);
}