/**
 * 1. Зробіть у файлі map додвання і видалення тултіпів над регіонами за допомогою класа hide.
 */

const region = document.querySelectorAll("path[data-region]");
console.log(region);

region.forEach(function (el) {
  let tooltip = document.createElement("span");
  tooltip.innerText = el.getAttribute("data-region");
  tooltip.classList.add("tooltip");
  tooltip.classList.add("hide");
  tooltip.setAttribute("data-region", el.getAttribute("data-region"))
  tooltip.style.top = el.nextElementSibling.getAttribute("cy") - 30 + "px";
  tooltip.style.left = el.nextElementSibling.getAttribute("cx") - 20 + "px";
  document.body.appendChild(tooltip);

})

region.forEach(function (el) {
  el.addEventListener("mouseover", showTooltip);
});

function showTooltip() {
  const tooltip = document.querySelectorAll(".tooltip");
  for (let i = 0; i < tooltip.length; i++) {
    if (this.getAttribute("data-region") == tooltip[i].getAttribute("data-region")) {
      tooltip[i].classList.remove("hide");
    }
  }
}

region.forEach(function (el) {
  el.addEventListener("mouseleave", hideTooltip);
});

function hideTooltip() {
  const tooltip = document.querySelectorAll(".tooltip");
  tooltip.forEach(function (el) {
    el.classList.add("hide");
  })
}